/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testcalculadora;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author El sag
 */
public class TestcalculadoraIT {
    
    Testcalculadora testt= new Testcalculadora();
    public TestcalculadoraIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testSuma() {
        
        int resultado=testt.suma(2,3);
        int esperado=5;
        assertEquals(esperado,resultado);
      
    }
    @Test
    public void testSumaConNegativo() {
        int result=testt.suma(6,-3);
        int esperado=3;
        assertEquals(esperado, result);
    }
    @Test
    public void testSumacero() {
        int result=testt.sumacero(9,0);
        int esperado=9;
        assertEquals(esperado, result);
    }
    @Test
    public void testResta() {
        int result=testt.resta(2,3);
        int esperado=1;
        assertEquals(esperado, result);
        
    }
    @Test
    public void testRestacero() {
         int result=testt.restacero(3,3);
        int esperado=0;
        assertEquals(esperado, result);
    }
    @Test
    public void testRestaSuma() {
        int result=testt.restasuma(3,-3);   
        int esperado=6;
        assertEquals(esperado, result);
    }
    @Test
    public void testMulti() {
        int result=testt.multi(2,3);
        int esperado=6;
        assertEquals(esperado, result);
        
    }
    @Test
    public void testMulticero() {
        int result=testt.multicero(3,0);
        int esperado=0;
        assertEquals(esperado, result);    
    }
    @Test
    public void testMultinegativo() {
        int result=testt.multinegativo(3,-3);
        int esperado=-9;
        assertEquals(esperado, result);
    }
    @Test
    public void testDivi() {
        int result=testt.divi(6,2);
        int esperado=3;
        assertEquals(esperado, result);
    }
}
